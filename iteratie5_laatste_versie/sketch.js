//voor het mooiste resultaat, laat de hele video eerst een maal afspelen en druk op de knop om hem weer te starten zodra het scherm zwart wordt

var audio;
var amplitude;
var button;

var video;

//een array van de hoogtes van de volumen in de loop van de tijd, zodat de vorige en de nieuwe volume hoogtes steeds getekend worden
var volumeGeschiedenis = [];

//de functie die het pauzeren en weer af laten spelen van de video en het geluid regelt
function toggleSong() {
  if (audio.isPlaying()) {
    audio.pause();
    button.html('play');
    video.pause();
  } else {
    audio.play();
    button.html('pause');
    video.play();
  }
}

//om er zeker van te zijn dat het geluid en de video optijd geladen zijn, worden ze voor de setup functie al in geladen
function preload() {
  audio = loadSound('insta.mp3');
  video = createVideo(['insta.mp4']);
}

function setup() {
  createCanvas(540,675);
  button = createButton('pause');
  button.mousePressed(toggleSong);
  amplitude = new p5.Amplitude();

  //deze timeout is toegevoegd om de audio en video tegelijkertijd af te laten spelen
  setTimeout(function(){
    audio.play();
  }, 300);
  video.play();
  //om de positie van de video te kunnen kiezen, hide ik de video hier en geef ik hem zijn gewenste positie later met image(...);
  video.hide();
}

function draw() {
  background(0);
  
  //hier kantel en verplaats ik het canvas, aangezien ik deze iteratie meer schermvullend wilde maken en ik de video dan full screen kan weergeven
  translate(width/2, 0);
  rotate(radians(90));
  
  //hier wordt de video met de gewenste grootte op de gewenste locatie weergegeven
  image(video, 0, -248);
  video.size(2900, 800);

  //hier wordt het nieuwe volume niveau opgevraagd en in de array met volume hoogtes toegevoegd
  var volume = amplitude.getLevel();
  volumeGeschiedenis.push(volume);
  //hier wordt de stroke ingesteld voor het tekenen van de visualisatie
  stroke(0);
  strokeWeight(3);
  strokeCap(ROUND);
  noFill();
  beginShape();
  //deze loop tekent de lijnen voor alle volume hoogtes in de array. Even later wordt hetzelfde gedaan maar dan voor de visualisatie aan de onderkant van de het beeld
  for (var i = 0; i < volumeGeschiedenis.length; i++) {
    //deze map zet de hoogte van het volume om naar een gewenste y locatie voor het begin/einde van de lijn
    var y = map(volumeGeschiedenis[i], 0, 1, -178, -518);
    line(i, -248, i, y);
  }
  endShape();

  beginShape();
  for (var i = 0; i < volumeGeschiedenis.length; i++) {
    var y = map(volumeGeschiedenis[i], 0, 1, 182, 522);
    line(i, 252, i, y);
  }
  endShape();
  
  //dit zorgt ervoor dat zodra de visualisaties het uiteinde van het beeld raken, de array met volume hoogtes aan de voorkant steeds één hoogte weghaalt
  //hierdoor zie je steeds de meest recente hoogtes die in het scherm passen
  if (volumeGeschiedenis.length > height+2) {
    volumeGeschiedenis.splice(0, 1);
  }
}