var song;
var amp;
var button;

var vid;

var volhistory = [];

function toggleSong() {
  if (song.isPlaying()) {
    song.pause();
    button.html('play');

    vid.pause();
  } else {
    song.play();
    button.html('pause');

    vid.play();
  }
}

function preload() {
  song = loadSound('insta.mp3');
  
  vid = createVideo(['insta.mp4']);
}

function setup() {
  createCanvas(540,675);
  button = createButton('pause');
  button.mousePressed(toggleSong);
  amp = new p5.Amplitude();

  setTimeout(function(){
    song.play();
  }, 300);
  vid.play();
  vid.hide();

  loadPixels();
}

function draw() {

  //code zoals ik de werking in gedachte heb

  // stroke(0);
  // rect(0, 0, 540, 675);

  // var midden = 420;
  // var vol = amp.getLevel();
  // volhistory.push(vol);
  // stroke(255);
  // strokeWeight(3);
  // strokeCap(ROUND);
  // noFill();
  // beginShape();
  // for (var i = 0; i < volhistory.length; i++) {
  //   var y = map(volhistory[i], 0, 1, midden, midden+270);
  //   line(i, midden, i, y);
  //   line(i, midden, i, midden-190);
  // }
  // endShape();
  
  // if (volhistory.length > width+2) {
  //   volhistory.splice(0, 1);
  // }

  // for (let x = 0; x < 540; x++) {
  //   for (let y = 0; y < 675; y++) {
  //     let loc = x + y * 540;
  //     let r, g, b;
  //     r = pixels[loc];
  //     if (r == 255){
  //       stroke(255, 0);
  //       pixels[loc] = 255;
  //     } else {
  //       stroke(0, 255);
  //       pixels[loc] = 0;
  //     }
  //   }
  // }

  // image(vid, 0, midden-221);

  // updatePixels();

  //voor werking hier, maar om te laten zien dat het te zwaar wordt boven image zetten. 




  //voer uit om te laten zien wat het idee zou zijn

  var midden = 420;
  background(0);

  image(vid, 0, midden-221);

  var vol = amp.getLevel();
  volhistory.push(vol);
  stroke(255);
  strokeWeight(3);
  strokeCap(ROUND);
  noFill();
  beginShape();
  for (var i = 0; i < volhistory.length; i++) {
    var y = map(volhistory[i], 0, 1, midden, midden+270);
    line(i, midden, i, y);
    line(i, midden, i, midden-190);
  }
  endShape();
  
  if (volhistory.length > width+2) {
    volhistory.splice(0, 1);
  }
}