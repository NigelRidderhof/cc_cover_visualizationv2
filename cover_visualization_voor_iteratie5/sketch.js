var song;
var amp;
var button;

var volhistory = [];

function toggleSong() {
  if (song.isPlaying()) {
    song.pause();
  } else {
    song.play();
  }
}

function preload() {
  song = loadSound('breathin.wav');
}

function setup() {
  createCanvas(540,675);
  button = createButton('toggle');
  button.mousePressed(toggleSong);
  song.play();
  amp = new p5.Amplitude();
}

function draw() {
  // background(0, 177, 64);
  background(0);

  var midden = 390;
  var vol = amp.getLevel();
  volhistory.push(vol);
  stroke(255);
  strokeWeight(3);
  strokeCap(ROUND);
  noFill();
  beginShape();
  for (var i = 0; i < volhistory.length; i++) {
    var y = map(volhistory[i], 0, 1, midden, midden+190);
    // var y2 = map(volhistory[i], 0, 1, midden, midden-130);
    // point(i, y);
    line(i, midden, i, y);
    // line(i, midden, i, y2);

    line(i, midden, i, midden-140);
  }
  endShape();
  
  if (volhistory.length > width+2) {
    volhistory.splice(0, 1);
  }

  // stroke(255);
  // strokeWeight(4);
  // strokeCap(ROUND);
  // line(volhistory.length,   midden+87, volhistory.length, midden-139);
  // line(volhistory.length+1, midden+87, volhistory.length+1, midden-139);
}