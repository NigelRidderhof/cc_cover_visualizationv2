var song;
var amp;
var button;

var vid;

var volhistory = [];

function toggleSong() {
  if (song.isPlaying()) {
    song.pause();
    button.html('play');

    vid.pause();
  } else {
    song.play();
    button.html('pause');

    vid.play();
  }
}

function preload() {
  song = loadSound('insta.mp3');
  
  vid = createVideo(['insta.mp4']);
}

function setup() {
  createCanvas(540,675);
  button = createButton('pause');
  button.mousePressed(toggleSong);
  amp = new p5.Amplitude();

  setTimeout(function(){
    song.play();
  }, 300);
  vid.play();
  vid.hide();

  loadPixels();
}

function draw() {
  background(255);
  var midden = 500;
  
  image(vid, 150, midden-321);
  fill(255);
  rect(0, midden-321, 540, 21);
  rect(0, midden, 540, 210);

  var vol = amp.getLevel();
  volhistory.push(vol);
  stroke(255);
  strokeWeight(3);
  strokeCap(ROUND);
  noFill();
  beginShape();
  for (var i = 0; i < volhistory.length; i++) {
    var y = map(volhistory[i], 0, 1, midden, midden-210);
    line(i, midden, i, y);
    line(i, midden, i, midden+10);
  }
  endShape();
  
  if (volhistory.length > width+2) {
    volhistory.splice(0, 1);
  }
}